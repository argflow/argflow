import numpy as np
import keras
import tensorflow as tf

from PIL import Image
from keras.applications.vgg16 import VGG16
from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input, decode_predictions

from argflow.influence import InfluenceGraph
from argflow.gaf import GAFExtractor, PayloadType, Payload
from argflow.gaf.frameworks import BipolarFramework
from argflow.gaf.default_mappers import DefaultConvolutionalStrengthMapper
from argflow.gaf.mappers import InfluenceMapper, CharacterisationMapper
from argflow.chi.cnn import GradCAM, ActMax
from argflow.chi import Chi
from argflow.portal import Writer


def default_decoder_function(preds, model, input):
    class_pred = model.predict_classes(input)
    certainty = model.predict_proba(input)
    return class_pred, certainty


class IM(InfluenceMapper):
    def __init__(self, no_final_block_filters=10, no_penultimate_block_filters=3, decoder_function=None):
        super().__init__()
        self.no_final_block_filters = no_final_block_filters
        self.no_penultimate_block_filters = no_penultimate_block_filters
        if decoder_function is None:
            self.decoder_function = None
        else:
            self.decoder_function = decoder_function

    def apply(self, model, x):
        influences = InfluenceGraph()

        # Obtain a list of the classifier layers (i.e. the ones after the final conv)
        classifier_layers = []
        for layer_last in reversed(model.layers):
            if isinstance(layer_last, keras.layers.Conv2D):
                break
            classifier_layers.append(layer_last)
        classifier_layers.reverse()

        # Obtain the last conv layers from the final two scales, as well as the layers between them
        last_block_conv = None
        penultimate_block_conv = None
        layers_between_penultimate_and_last = []

        # Since we'll usually have a nubmer of convolutions followed by a max pool,
        # we can detect the final conv layers in the last two blocks thus (can probably be refactored)
        seen_max_pool = False
        for layer_last in reversed(model.layers):
            if isinstance(layer_last, keras.layers.Conv2D) and last_block_conv is None:
                last_block_conv = layer_last
            if isinstance(layer_last, keras.layers.MaxPool2D) and not last_block_conv is None:
                seen_max_pool = True
            if isinstance(layer_last, keras.layers.Conv2D) and seen_max_pool:
                penultimate_block_conv = layer_last
                break
            if not last_block_conv is None:
                layers_between_penultimate_and_last.append(layer_last)

        # Get the relevant features in the final conv layer
        relevant_features_last = self.get_relevant_last(
            model, x, last_block_conv, classifier_layers
        )

        relevant_features_penultimate = self.get_relevant_penultimate(
            model, x, last_block_conv, penultimate_block_conv, relevant_features_last, layers_between_penultimate_and_last
        )

        # Fetch and decode model prediction
        preds = model.predict(x)
        influences.add_node('Prediction', grad=1)
        if self.decoder_function is not None:
            _, predicted_class, confidence = decode_predictions(preds, top=1)[
                0][0]
        else:
            predicted_class, confidence = default_decoder_function(
                preds, model, x)

        # Construct influence graph
        influences.add_node('Input', grad=0)

        for layer_last, idx_last, grad_last in relevant_features_last:
            influences.add_node(
                f'Filter {idx_last} of {layer_last}', layer=layer_last, filter_idx=idx_last, grad=grad_last
            )
            influences.add_influence(f'Filter {idx_last} of {layer_last}', 'Prediction')
            # Add filters from penultimate block conv layer that contribute to this particular filter
            for layer_penultimate, idx_penultimate, grad_penultimate in relevant_features_penultimate[idx_last]:
                influences.add_node(
                    f'Filter {idx_penultimate} of {layer_penultimate}',
                    layer=layer_penultimate, filter_idx=idx_penultimate, grad=grad_penultimate
                )
                influences.add_influence(
                    'Input', f'Filter {idx_penultimate} of {layer_penultimate}'
                )
                influences.add_influence(
                    f'Filter {idx_penultimate} of {layer_penultimate}', f'Filter {idx_last} of {layer_last}'
                )

        return influences, predicted_class, confidence

    def get_relevant_last(self, model, x, last_conv_layer, classifier_layers):
        last_conv_layer_model = keras.Model(
            model.inputs, last_conv_layer.output)

        # Create a model that maps the activations of the last conv
        # layer to the final class predictions
        classifier_input = keras.Input(shape=last_conv_layer.output.shape[1:])
        y = classifier_input
        for layer in classifier_layers:
            y = layer(y)
        classifier_model = keras.Model(classifier_input, y)

        # Compute the gradient of the top predicted class for our input image
        # with respect to the activations of the last conv layer
        with tf.GradientTape() as tape:
            # Compute activations of the last conv layer and make the tape watch it
            last_conv_layer_output = last_conv_layer_model(x)
            tape.watch(last_conv_layer_output)
            # Compute class predictions
            preds = classifier_model(last_conv_layer_output)
            top_pred_index = tf.argmax(preds[0])
            top_class_channel = preds[:, top_pred_index]

        # This is the gradient of the top predicted class with regard to
        # the output feature map of the last conv layer
        grads = tape.gradient(top_class_channel, last_conv_layer_output)

        # This is a vector where each entry is the mean intensity of the gradient
        # over a specific feature map channel
        pooled_grads = tf.reduce_mean(grads, axis=(0, 1, 2))

        last_conv_layer_output = last_conv_layer_output.numpy()[0]
        pooled_grads = pooled_grads.numpy()
        # Sort filters by contribution and return list of (layer, filter index, gradient) tuples
        top_pooled_grads = [(last_conv_layer.name, i, pooled_grads[i])
                            for i in (np.abs(pooled_grads)).argsort()[-self.no_final_block_filters:]]

        return top_pooled_grads

    def get_relevant_penultimate(self, model, x, last_conv_layer, penultimate_conv_layer, last_relevant, intervening_layers):
        penultimate_conv_layer_model = keras.Model(
            model.inputs, penultimate_conv_layer.output)

        # Create a model that maps the activations of the penultimate block conv
        # layer to the last block conv layer
        intervening_input = keras.Input(
            shape=penultimate_conv_layer.output.shape[1:])
        y = intervening_input
        for layer in intervening_layers:
            y = layer(y)
        intervening_model = keras.Model(intervening_input, y)

        # Create a dict to store gradients for each item
        filter_grads = {}
        # Compute the gradient of the most relevant last block conv layer filters
        # with respect to the activations of the penultimate block conv layer
        for _, idx, _ in last_relevant:
            with tf.GradientTape() as tape:
                # Compute activations of the penultimate block conv layer and make the tape watch it
                penultimate_conv_layer_output = penultimate_conv_layer_model(x)
                tape.watch(penultimate_conv_layer_output)
                # Compute last conv layer output
                last_conv_layer_output = intervening_model(
                    penultimate_conv_layer_output)
                # Extract relevant filter from last block conv layer
                feature_map = last_conv_layer_output[:, :, :, idx]
                # This is the gradient of the filter in the last conv block layer wrt the penultimate conv
                # block layer
                grads = tape.gradient(
                    feature_map, penultimate_conv_layer_output)
                # This is a vector where each entry is the mean intensity of the gradient
                # over a specific feature map channel
                pooled_grads = tf.reduce_mean(grads, axis=(0, 1, 2))
                pooled_grads = pooled_grads.numpy()
                # Sort filters by contribution and return list of (layer, filter index, gradient) tuples
                top_pooled_grads = [(penultimate_conv_layer.name, i, pooled_grads[i])
                                    for i in (np.abs(pooled_grads)).argsort()[-self.no_penultimate_block_filters:]]
                # Save top k to dict
                filter_grads[idx] = top_pooled_grads

        return filter_grads


class CM(CharacterisationMapper):
    def apply(self, u, v):
        return BipolarFramework.ATTACK if u['grad'] < 0 else BipolarFramework.SUPPORT

class GradCAMWithActMax(Chi):
    def generate(self, x, node, model):
        gc = GradCAM()
        am = ActMax()
        img_gc = gc.generate(x, node, model).content
        img_am = am.generate(x, node, model).content
        return Payload((img_gc, img_am), PayloadType.IMAGE_PAIR)


if __name__ == '__main__':
    model = VGG16(weights='imagenet')
    summaries = Writer('../portal/examples', 'CNN 2')

    img_path = 'demos/tiger.jpg'
    img = image.load_img(img_path, target_size=(224, 224))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)

    im = IM(3, 5, decode_predictions)
    cm = CM()
    sm = DefaultConvolutionalStrengthMapper()

    extractor = GAFExtractor(
        im, sm, cm, GradCAMWithActMax())
    gaf = extractor.extract(model, x)
    summaries.write_gaf(gaf)
